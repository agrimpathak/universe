
#include <iostream>
#include <memory>
#include <string>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "physics.h"
#include "io.h"

constexpr float GRAVITATIONAL_CONSTANT = 1.e-9f;

template<size_t N_POINTS, size_t N_DIM>
void play(float* const pos,
          float* const vel,
          float* const acc);

#include "play.hpp"
