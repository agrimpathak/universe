#ifndef FACTORY_H
#define FACTORY_H

#include <iostream>
#include <algorithm>
#include <cmath>
#include "physics.h"
#include "rng.h"

template<size_t N>
inline
void rand_arr(float* const arr,
              const float a,
              const float b)
{
        UniformRealRng rng(a, b, rand());
        std::generate(arr, arr + N, std::ref(rng));
}


/* RANDOM SPHERE */

template<size_t DIM>
inline
void rand_pt_sphere(float* const pos,
                    const float* const center,
                    const float radius);

template<>
inline
void rand_pt_sphere<2>(float* const pos,
                       const float* const center,
                       const float radius)
{
        UniformFloatRng rng_rad(-radius, +radius, rand());
        UniformFloatRng rng_ang(0.0f, +1.0f, rand());
        const float rad = rng_rad();
        const float ang = rng_ang() * static_cast<float>(M_PI);
        pos[0] = center[0] + rad * std::cos(ang);
        pos[1] = center[1] + rad * std::sin(ang);
}


// Technically, may not be truly random
template<>
inline
void rand_pt_sphere<3>(float* const pos,
                       const float* const center,
                       const float radius)
{
        UniformRealRng rng(-radius, +radius);
        const float r_cubed = static_cast<float>(pow(radius, 3));
        const float origin[3] {0.0f, 0.0f, 0.0f};
        do
        {
                std::generate(pos, pos + 3, std::ref(rng));
        } while(distance_squared<3>(pos, origin) <= r_cubed);
        pos[0] += center[0];
        pos[1] += center[1];
        pos[2] += center[2];
}


template<size_t N_POINTS, size_t DIM>
inline
void init_rand_sphere(float* pos,
                      const float* const center,
                      const float radius)
{
        for (size_t i = 0; i < N_POINTS; ++i, pos += DIM)
        {
                rand_pt_sphere<DIM>(pos, center, radius);
        }
}


#endif // FACTORY_H
