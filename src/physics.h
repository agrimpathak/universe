#ifndef PHYSICS_H
#define PHYSICS_H

// y += x
template<unsigned int PTS_x_DIM>
inline
void vec_add(      float* const y,
             const float* const x)
{
        for (size_t i = 0; i < PTS_x_DIM; ++i)
        {
                y[i] += x[i];
        }
}


template<unsigned int PTS_x_DIM>
inline
void update_pos(      float* const pos,
                const float* const vel)
{
        vec_add<PTS_x_DIM>(pos, vel);
}


template<unsigned int PTS_x_DIM>
inline
void update_vel(      float* const vel,
                const float* const acc)
{
        vec_add<PTS_x_DIM>(vel, acc);
}


template<typename T>
inline
T square(T x)
{
        return x * x;
}


template<unsigned int N_DIM>
inline
float distance_squared(const float* const pt_a,
                       const float* const pt_b);


template<>
inline
float distance_squared<2>(const float* const pt_a,
                          const float* const pt_b)
{
        return square(pt_a[0] - pt_b[0]) +
               square(pt_a[1] - pt_b[1]);
}


template<>
inline
float distance_squared<3>(const float* const pt_a,
                          const float* const pt_b)
{
        return square(pt_a[0] - pt_b[0]) +
               square(pt_a[1] - pt_b[1]) +
               square(pt_a[2] - pt_b[2]);
}


// Assumes all masses are 1.0
template<unsigned int N_DIM>
inline
float gravity(const float* const pt_a,
              const float* const pt_b,
              const float G)
{
        return G / distance_squared<N_DIM>(pt_a, pt_b);
}


template<unsigned int N_DIM>
inline
void update_pt_acc(const float* const pos_i,
                   const float* const pos_j,
                         float* const acc_i,
                         float* const acc_j,
                   const float grav);


template<>
inline
void update_pt_acc<2>(const float* const pos_i,
                      const float* const pos_j,
                            float* const acc_i,
                            float* const acc_j,
                      const float grav)
{
        const float g_dx = grav * (pos_i[0] - pos_j[0]);
        const float g_dy = grav * (pos_i[1] - pos_j[1]);
        acc_i[0] -= g_dx;
        acc_i[1] -= g_dy;
        acc_j[0] += g_dx;
        acc_j[1] += g_dy;
}


template<>
inline
void update_pt_acc<3>(const float* const pos_i,
                      const float* const pos_j,
                            float* const acc_i,
                            float* const acc_j,
                      const float grav)
{
        const float g_dx = grav * (pos_i[0] - pos_j[0]);
        const float g_dy = grav * (pos_i[1] - pos_j[1]);
        const float g_dz = grav * (pos_i[2] - pos_j[2]);
        acc_i[0] -= g_dx;
        acc_i[1] -= g_dy;
        acc_i[2] -= g_dz;
        acc_j[0] += g_dx;
        acc_j[1] += g_dy;
        acc_j[2] += g_dz;
}


template<size_t PTS_x_DIM, unsigned int N_DIM>
inline
void update_acc(      float* const acc,
                const float* const pos,
                const float G)
{
        std::fill(acc, acc + PTS_x_DIM, 0.0f);
        for(size_t i = 0; i < PTS_x_DIM; i += N_DIM)
        {
                const float* const pos_i = pos + i;
                      float* const acc_i = acc + i;
                for(size_t j = i + N_DIM; j < PTS_x_DIM; j += N_DIM)
                {
                        const float* const pos_j = pos + j;
                              float* const acc_j = acc + j;
                        const float g = gravity<N_DIM>(pos_i, pos_j, G);
                        update_pt_acc<N_DIM>(pos_i, pos_j, acc_i, acc_j, g);
                }
        }
}

#endif // PHYSICS_H
