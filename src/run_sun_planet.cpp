
#include "block.h"
#include "factory.h"
#include "play.h"

constexpr size_t N_POINTS    = 1 << 12;
constexpr unsigned int N_DIM = 2;
constexpr size_t PTS_x_DIM   = N_POINTS * N_DIM;

int main()
{

        Block<float> flt_mem;
        flt_mem.init(3 * PTS_x_DIM);
        float* const pos = flt_mem.alloc_block(PTS_x_DIM);
        float* const vel = flt_mem.alloc_block(PTS_x_DIM);
        float* const acc = flt_mem.alloc_block(PTS_x_DIM);

        float* pos_ = pos;
        float* vel_ = vel;
        float* acc_ = acc;

        constexpr size_t N_POINTS_PLANET = 1 << 6;
        const float planet_init_vel = +1e-3f; // will set both x and y

        const float planet_init_pos1[2] {0.0f, -0.5f};
        init_rand_sphere<N_POINTS_PLANET, N_DIM>(pos_, planet_init_pos1, 0.005f);
        std::fill(vel_, vel_ + N_POINTS_PLANET * N_DIM, planet_init_vel);
        std::fill(acc_, acc_ + N_POINTS_PLANET * N_DIM, 0.0f);

        pos_ += N_POINTS_PLANET * N_DIM;
        vel_ += N_POINTS_PLANET * N_DIM;
        acc_ += N_POINTS_PLANET * N_DIM;

        const float planet_init_pos2[2] {-0.5f, 0.0f};
        init_rand_sphere<N_POINTS_PLANET, N_DIM>(pos_, planet_init_pos2, 0.005f);
        std::fill(vel_, vel_ + N_POINTS_PLANET * N_DIM, planet_init_vel);
        std::fill(acc_, acc_ + N_POINTS_PLANET * N_DIM, 0.0f);

        pos_ += N_POINTS_PLANET * N_DIM;
        vel_ += N_POINTS_PLANET * N_DIM;
        acc_ += N_POINTS_PLANET * N_DIM;

        constexpr size_t N_POINTS_SUN = N_POINTS - 2 * N_POINTS_PLANET;
        const float sun_init_pos[2] { 0.0f, 0.0f };

        init_rand_sphere<N_POINTS_SUN, N_DIM>(pos_, sun_init_pos, 0.005f);
        std::fill(vel_, vel_ + N_POINTS_SUN * N_DIM, 0.0f);
        std::fill(acc_, acc_ + N_POINTS_SUN * N_DIM, 0.0f);

        play<N_POINTS, N_DIM>(pos, vel, acc);
        return 0;
}
