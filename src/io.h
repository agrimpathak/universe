#ifndef IO_H
#define IO_H

#include <fstream>
#include <sstream>
#include <string>

std::string read_file(const std::string& file_path);

#endif // IO_H
