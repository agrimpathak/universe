
#include "block.h"
#include "factory.h"
#include "play.h"

constexpr unsigned int N_DIM = 2;
constexpr size_t N_POINTS    = 1 << 12;
constexpr size_t PTS_x_DIM   = N_POINTS * N_DIM;

int main()
{

        Block<float> flt_mem;
        flt_mem.init(3 * PTS_x_DIM);
        float* const pos = flt_mem.alloc_block(PTS_x_DIM);
        float* const vel = flt_mem.alloc_block(PTS_x_DIM);
        float* const acc = flt_mem.alloc_block(PTS_x_DIM);

        const float center[2] {0.0f, 0.0f};
        init_rand_sphere<N_POINTS, N_DIM>(pos, center, 0.5f);
        std::fill(vel, vel + PTS_x_DIM, 0.0f);
        std::fill(acc, acc + PTS_x_DIM, 0.0f);
        play<N_POINTS, N_DIM>(pos, vel, acc);
        return 0;
}
