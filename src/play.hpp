
GLuint compile_shader(GLuint type, const std::string& source)
{
        const GLuint id = glCreateShader(type);
        const char* const src = source.c_str();
        glShaderSource(id, 1, &src, nullptr);
        glCompileShader(id);
        int result;
        glGetShaderiv(id, GL_COMPILE_STATUS, &result);
        if (result == GL_FALSE)
        {
                int length;
                glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
                std::unique_ptr<char[]> msg_hdl (new char[length]);
                char* const msg = msg_hdl.get();
                glGetShaderInfoLog(id, length, &length, msg);
                std::cout << "Failed to compile ";
                std::cout << (type == GL_VERTEX_SHADER ? "vertex" : "fragment");
                std::cout << " shader\n" << msg << "\n";

        }
        return id;
}


GLuint create_shader(const std::string& vertex_shader,
                     const std::string& fragment_shader)
{
        GLuint program = glCreateProgram();
        GLuint vs = compile_shader(GL_VERTEX_SHADER,   vertex_shader);
        GLuint fs = compile_shader(GL_FRAGMENT_SHADER, fragment_shader);
        glAttachShader(program, vs);
        glAttachShader(program, fs);
        glLinkProgram(program);
        glValidateProgram(program);
        glDetachShader(vs, GL_VERTEX_SHADER);
        glDetachShader(fs, GL_FRAGMENT_SHADER);
        return program;
}


template<size_t N_POINTS, size_t N_DIM>
void play(float* const pos,
          float* const vel,
          float* const acc)
{
        constexpr size_t PTS_x_DIM = N_POINTS * N_DIM;
        if(!glfwInit())
        {
                throw std::runtime_error("");
        }

        GLFWwindow* const window = glfwCreateWindow(1920, 1080, "Universe",
                                                    NULL, NULL);

        if (!window)
        {
                std::cout << "Unable to create window\n";
                glfwTerminate();
                throw std::runtime_error("");
        }

        glfwMakeContextCurrent(window);

        if (glewInit() != GLEW_OK)
        {
                std::cout << "glewInit() != GLEW_OK\n";
                throw std::runtime_error("");
        }

        GLuint buffer_id;
        glGenBuffers(1, &buffer_id);
        glBindBuffer(GL_ARRAY_BUFFER, buffer_id);

        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, N_DIM, GL_FLOAT, GL_FALSE,
                              sizeof(float) * N_DIM, 0);
        const std::string vert_shader = read_file("../res/shader/basic.vs");
        const std::string frag_shader = read_file("../res/shader/basic.fs");
        GLuint shader = create_shader(vert_shader, frag_shader);
        glUseProgram(shader);

        while(!glfwWindowShouldClose(window))
        {
                glClear(GL_COLOR_BUFFER_BIT);
                glBufferData(GL_ARRAY_BUFFER,
                             N_POINTS * sizeof(float),
                             pos,
                             GL_DYNAMIC_DRAW);
                glDrawArrays(GL_POINTS, 0, N_POINTS);
                glfwSwapBuffers(window);
                glfwPollEvents();

                update_acc<PTS_x_DIM, N_DIM>(acc, pos, GRAVITATIONAL_CONSTANT);
                update_vel<PTS_x_DIM>(vel, acc);
                update_pos<PTS_x_DIM>(pos, vel);
        }

        glDeleteProgram(shader);
        glfwTerminate();
}
