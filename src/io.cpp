#include "io.h"

std::string read_file(const std::string& file_path)
{
        std::ifstream stream(file_path);
        std::string line;
        std::stringstream ss;
        while(getline(stream, line))
        {
                ss << line << "\n";
        }
        return ss.str();
}
